package com.ucbcba.MedicalHistory.Services;

import com.ucbcba.MedicalHistory.Entities.MedicalRecord;
import com.ucbcba.MedicalHistory.Repository.MedicalRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class MedicalRecordServiceImpl implements MedicalRecordService {

    @Autowired
    private MedicalRecordRepository medicalRecordRepository;

    @Autowired
    @Qualifier(value = "medicalRecordRepository")

    public void setUserRepository(MedicalRecordRepository medicalRecordRepository) {
        this.medicalRecordRepository = medicalRecordRepository;
    }

    @Override
    public Iterable<MedicalRecord> listAll() {
        return medicalRecordRepository.findAll();
    }

    @Override
    public void save(MedicalRecord consulta) {
        medicalRecordRepository.save(consulta);
    }

    @Override
    public MedicalRecord findById(Integer id) {
        return medicalRecordRepository.findById(id).get();
    }

    @Override
    public void delete(Integer id) {
        medicalRecordRepository.deleteById(id);
    }

}
