package com.ucbcba.MedicalHistory.Services;
import com.ucbcba.MedicalHistory.Entities.User;
public interface UserService {
    void save(User user);
    User findByUsername(String username);
    User findById(Integer id);
}
