package com.ucbcba.MedicalHistory.Services;

import java.util.Date;
import java.util.List;

import com.ucbcba.MedicalHistory.Entities.MedicalRecord;

public interface MedicalRecordService {

    Iterable<MedicalRecord> listAll();

    void save(MedicalRecord consulta);

    MedicalRecord findById(Integer id);

    void delete(Integer id);

}
