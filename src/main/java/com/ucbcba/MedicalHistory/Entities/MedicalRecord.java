package com.ucbcba.MedicalHistory.Entities;

import org.springframework.format.annotation.DateTimeFormat;
import sun.util.calendar.BaseCalendar;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class MedicalRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String medspec;

    private String namemed;

    private String severity;

    private String description;

    private String treatment;

    private String analysis;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    
    private Date date;

    public Integer getId() {
        return id;
    }

    /**
     * @return the treatment
     */
    public String getTreatment() {
        return treatment;
    }

    /**
     * @param treatment the treatment to set
     */
    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }

    /**
     * @return the severity
     */
    public String getSeverity() {
        return severity;
    }

    /**
     * @param severity the severity to set
     */
    public void setSeverity(String severity) {
        this.severity = severity;
    }

    /**
     * @return the namemed
     */
    public String getNamemed() {
        return namemed;
    }

    /**
     * @param namemed the namemed to set
     */
    public void setNamemed(String namemed) {
        this.namemed = namemed;
    }

    /**
     * @return the medspec
     */
    public String getMedspec() {
        return medspec;
    }

    /**
     * @param medspec the medspec to set
     */
    public void setMedspec(String medspec) {
        this.medspec = medspec;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    
    /**
     * @return the analysis
     */
    public String getAnalysis() {
        return analysis;
    }

    /**
     * @param analysis the analysis to set
     */
    public void setAnalysis(String analysis) {
        this.analysis = analysis;
    }
}