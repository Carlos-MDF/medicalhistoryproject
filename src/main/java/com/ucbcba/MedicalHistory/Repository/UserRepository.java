package com.ucbcba.MedicalHistory.Repository;

import com.ucbcba.MedicalHistory.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends JpaRepository<User,Integer> {
    User findByUsername(String username);
}
