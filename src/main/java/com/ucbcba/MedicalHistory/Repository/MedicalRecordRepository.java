package com.ucbcba.MedicalHistory.Repository;

import com.ucbcba.MedicalHistory.Entities.MedicalRecord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Transactional
public interface MedicalRecordRepository extends CrudRepository<MedicalRecord, Integer> {
}