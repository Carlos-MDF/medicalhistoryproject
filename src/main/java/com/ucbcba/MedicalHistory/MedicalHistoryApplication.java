package com.ucbcba.MedicalHistory;

import java.io.File;

import com.ucbcba.MedicalHistory.Controllers.FileUploadController;
import com.ucbcba.MedicalHistory.Services.StorageProperties;
import com.ucbcba.MedicalHistory.Services.StorageService;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan({"com.ucbcba.MedicalHistory","Controllers"})
@EnableConfigurationProperties(StorageProperties.class)
public class MedicalHistoryApplication {

	public static void main(String[] args) {
        new File(FileUploadController.uploadDirectory).mkdir();
		SpringApplication.run(MedicalHistoryApplication.class, args);
	}
	// @Bean
    // CommandLineRunner init(StorageService storageService) {
    //     return (args) -> {
    //         storageService.deleteAll();
    //         storageService.init();
    //     };
    // }

}
