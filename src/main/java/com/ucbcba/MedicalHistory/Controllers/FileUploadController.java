package com.ucbcba.MedicalHistory.Controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.ucbcba.MedicalHistory.Entities.MedicalRecord;
import com.ucbcba.MedicalHistory.Services.MedicalRecordService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {
  public static String uploadDirectory = System.getProperty("user.dir")+"\\src\\main\\resources\\static\\images\\";
  @Autowired
  private MedicalRecordService medicalRecordService;
  @RequestMapping("/")
  public String UploadPage(Model model) {
	  return "medicalRecord";
  }
  @RequestMapping("/upload")
  public String upload(Model model,@RequestParam("files") MultipartFile[] files, @ModelAttribute("medicalRecord") MedicalRecord medicalRecord) {
      System.out.println("---------------------------");
      System.out.println(uploadDirectory);
      medicalRecord.setAnalysis("aaa");
	  StringBuilder fileNames = new StringBuilder();
	  for (MultipartFile file : files) {
		  Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
		  fileNames.append(file.getOriginalFilename()+" ");
		  try {
			Files.write(fileNameAndPath, file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	  }
	  model.addAttribute("msg", "Successfully uploaded files "+fileNames.toString());
	  return "uploadstatusview";
  }
  
  
}