package com.ucbcba.MedicalHistory.Controllers;

import com.ucbcba.MedicalHistory.Entities.MedicalRecord;
import com.ucbcba.MedicalHistory.Services.MedicalRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Controller
public class MedicalHistoryController {

    @Autowired
    private MedicalRecordService medicalRecordService;

    public static String uploadDirectory = System.getProperty("user.dir")+"\\src\\main\\resources\\static\\images\\";

    private String miRuta = "http://localhost:8090/images/";

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        List<MedicalRecord> medicalrecord = (List) medicalRecordService.listAll();
        model.addAttribute("medicalRecord", medicalrecord);

        return "medicalRecord";
    }

    @RequestMapping(value = "/showMedicalRecord/{id}", method = RequestMethod.GET)
    public String show(@PathVariable Integer id, Model model) {
        MedicalRecord med = medicalRecordService.findById(id);
        model.addAttribute("medicalRecord", med);
        return "showMedicalRecord";
    }

    @RequestMapping(value = "/ascent", method = RequestMethod.GET)
    public String ascent(Model model) {
        List<MedicalRecord> medicalrecord = (List) medicalRecordService.listAll();
        for(int i=0;i<medicalrecord.size();i++){
            for(int j=0;j<medicalrecord.size();j++){
                if(medicalrecord.get(i).getDate()!=null && medicalrecord.get(j).getDate()!=null){
                    if(medicalrecord.get(i).getDate().before(medicalrecord.get(j).getDate())){
                        MedicalRecord aux=medicalrecord.get(i);
                        medicalrecord.set(i,medicalrecord.get(j));
                        medicalrecord.set(j,aux);
                       }
                }
            }
        }
        model.addAttribute("medicalRecord", medicalrecord);

        return "ascent";
    }

    @RequestMapping(value = "/descent", method = RequestMethod.GET)
    public String descent(Model model) {
        List<MedicalRecord> medicalrecord = (List) medicalRecordService.listAll();
        for(int i=0;i<medicalrecord.size();i++){
            for(int j=0;j<medicalrecord.size();j++){
                if(medicalrecord.get(i).getDate()!=null && medicalrecord.get(j).getDate()!=null){
                    if(medicalrecord.get(i).getDate().after(medicalrecord.get(j).getDate())){
                        MedicalRecord aux=medicalrecord.get(i);
                        medicalrecord.set(i,medicalrecord.get(j));
                        medicalrecord.set(j,aux);
                       }
                }
            }
        }
        model.addAttribute("medicalRecord", medicalrecord);

        return "descent";
    }


    @RequestMapping (value = "/newMedicalRecord")
    public String createMedicalRecord(Model model)
    {
        model.addAttribute("medicalRecord",new MedicalRecord());
        return "newMedicalRecord";
    }

    @RequestMapping (value = "/createmedicalRecord", method = RequestMethod.POST)
    public String create(@ModelAttribute("medicalRecord") MedicalRecord medicalRecord, Model model, @RequestParam("files") MultipartFile[] files)
    {
	  StringBuilder fileNames = new StringBuilder();
	  for (MultipartFile file : files) {
		  Path fileNameAndPath = Paths.get(uploadDirectory, file.getOriginalFilename());
		  fileNames.append(file.getOriginalFilename()+" ");
		  try {
            medicalRecord.setAnalysis(miRuta + file.getOriginalFilename());
			Files.write(fileNameAndPath, file.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
      }
	    medicalRecordService.save(medicalRecord);
        return "redirect:/";
    }

    @RequestMapping (value = "/editMedicalRecord/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable Integer id, Model model)
    {
        MedicalRecord m=medicalRecordService.findById(id);
        model.addAttribute("medicalRecord",m);
        return "editMedicalRecord";
    }

    @RequestMapping(value = "/deleteMedicalRecord/{id}",method = RequestMethod.GET )
    public String delete (@PathVariable Integer id,Model model)
    {
        medicalRecordService.delete(id);
        return "redirect:/";
    }
}